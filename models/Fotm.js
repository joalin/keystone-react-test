var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Fotm = new keystone.List('Fotm', {
    defaultSort: '-publishedAt'
});

Fotm.add({
    name: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
    createdAt: { type: Date, default: Date.now },
    publishedAt: Date,
    content: {
        about: { type: Types.Textarea, height: 150 },
        firstJob: { type: Types.Textarea, height: 150 },
        latestJob: { type: Types.Textarea, height: 150 },
        future: { type: Types.Textarea, height: 150 }
    }
});

Fotm.defaultColumns = 'name'
Fotm.register();

var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Translation = new keystone.List('Translation', {
    autokey: { path: 'slug', from: 'key', unique: true },
    map: { name: 'key' },
    defaultSort: '+key'
});

Translation.add({
    key: { type: String, required: true },
    sv_SE: { type: String, initial: false },
    en_EN: { type: String, initial: false }
});

Translation.defaultColumns = 'key, sv_SE, en_EN'
Translation.register();

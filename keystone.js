var keystone = require('keystone');

keystone.init({

	'name': 'keystone-react-test',
	'brand': 'keystone-react-test',

	'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'jade',

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': '4Bx6lR+1Lh?;V&amp;8QTm3(3mLm,XQ44p[U:&#34;ukVTrQ#2JN5x!7^*6`CZPJm8:D.,&amp;O',

});

keystone.import('models');

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));
keystone.set('nav', {
	'users': 'users',
	'translations': 'translations'
});

keystone.start();

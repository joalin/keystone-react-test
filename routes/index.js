var babelify = require('babelify');
var browserify = require('browserify-middleware');
var keystone = require('keystone');

var importRoutes = keystone.importer(__dirname);

// Setup Route Bindings
exports = module.exports = function(app) {

	app.use('/js', browserify('./client/scripts', {
		transform: [babelify.configure({
			plugins: ['object-assign']
		})]
	}));

	app.use('/posts', function(req, res) {
		var Post = keystone.list('Post');
		res.setHeader('Content-Type', 'application/json');
		Post.model.find().limit(100).exec(function(err, posts) {
			res.send(JSON.stringify(posts));
		});
	});

	// Views
	app.use(function(req, res) {

		var Post = keystone.list('Post');
		Post.model.find().exec(function(err, posts) {
			res.render('index', { posts: posts });
		});

	});

};
